package com.moravskiyandriy.poTest;

import com.moravskiyandriy.pageobjects.implementations.GmailPage;
import com.moravskiyandriy.pageobjects.implementations.ReverseActionPopUp;
import com.moravskiyandriy.utils.DriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;

import static org.testng.Assert.assertEquals;

public class DeleteLettersAndReverseActionTest {
    private static final Logger logger = LogManager.getLogger(DeleteLettersAndReverseActionTest.class);
    private static final String DEFAULT_ACCOUNT_ADDRESS ="NickScorpi@gmail.com";
    private static final String DEFAULT_ACCOUNT_PASSWORD="sc0rp198";
    private static final int DEFAULT_QUANTITY_OF_LETTERS_TO_DELETE=3;

    @Test
    void deleteLettersAndReverseAction() {
        int lettersQuantityBeforeDeletion;
        int lettersQuantityAfterDeletion;
        GmailPage gmailPage = new GmailPage();
        gmailPage.goToGmailPage();
        gmailPage.fillLoginField(Optional.ofNullable(getProperties()
                .getProperty("ACCOUNT_ADDRESS"))
                .orElse(DEFAULT_ACCOUNT_ADDRESS));
        gmailPage.submitLogin();
        gmailPage.fillPasswordField(Optional.ofNullable(getProperties()
                .getProperty("ACCOUNT_PASSWORD"))
                .orElse(DEFAULT_ACCOUNT_PASSWORD));
        gmailPage.submitPassword();
        gmailPage.clickIncomingLettersButton();
        lettersQuantityBeforeDeletion = gmailPage.countIncomingLetters();
        gmailPage.checkLettersForDeletion(Optional.ofNullable(getProperties()
                .getProperty("QUANTITY_OF_LETTERS_TO_DELETE"))
                .map(Integer::valueOf)
                .orElse(DEFAULT_QUANTITY_OF_LETTERS_TO_DELETE));
        gmailPage.clickDeleteButton();
        ReverseActionPopUp reverseActionPopUp = new ReverseActionPopUp();
        reverseActionPopUp.clickUndoButton();
        gmailPage.clickIncomingLettersButton();
        lettersQuantityAfterDeletion = gmailPage.countIncomingLetters();
        DriverManager.quitDriver();
        assertEquals(lettersQuantityAfterDeletion,lettersQuantityBeforeDeletion);
    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = DeleteLettersAndReverseActionTest.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        return prop;
    }
}
