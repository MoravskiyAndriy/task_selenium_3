package com.moravskiyandriy.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class DriverManager {
    static{
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
    }
    private static WebDriver driver;

    private DriverManager(){}

    public static WebDriver getDriver(){
        if(Objects.isNull(driver)){
            driver=new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        }
        return driver;
    }

    public static void quitDriver(){
        driver.quit();
    }
}
